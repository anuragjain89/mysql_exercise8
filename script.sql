DROP DATABASE IF EXISTS employee_commission;
CREATE DATABASE employee_commission;
USE employee_commission;

DROP TABLE IF EXISTS Departments;
CREATE TABLE Departments (
  id INT AUTO_INCREMENT,
  name VARCHAR(20),
  PRIMARY KEY(id)
);

DROP TABLE IF EXISTS Employees;
CREATE TABLE Employees (
  id INT AUTO_INCREMENT,
  name VARCHAR(20),
  salary DECIMAL(12,2),
  department_id INT,
  PRIMARY KEY(id),
  INDEX(department_id)
);

DROP TABLE IF EXISTS Commissions;
CREATE TABLE Commissions (
  id INT AUTO_INCREMENT,
  employee_id INT,
  commission_amount DECIMAL(12,2),
  PRIMARY KEY(id),
  INDEX(commission_amount)
);

ALTER TABLE Employees
ADD CONSTRAINT fk_EmployeesDepartments FOREIGN KEY(department_id) REFERENCES Departments(id);

ALTER TABLE Commissions
ADD CONSTRAINT fk_CommissionsEmployees FOREIGN KEY(employee_id) REFERENCES Employees(id);

INSERT INTO Departments(name) VALUES ('Banking');
INSERT INTO Departments(name) VALUES ('Insurance');
INSERT INTO Departments(name) VALUES ('Services');

INSERT INTO Employees(name, salary, department_id) VALUES ('Chris Gayle', 1000000, 1);
INSERT INTO Employees(name, salary, department_id) VALUES ('Michael Clarke', 800000, 2);
INSERT INTO Employees(name, salary, department_id) VALUES ('Rahul Dravid', 700000, 1);
INSERT INTO Employees(name, salary, department_id) VALUES ('Ricky Pointing', 600000, 2);
INSERT INTO Employees(name, salary, department_id) VALUES ('Albie Morkel', 650000, 2);
INSERT INTO Employees(name, salary, department_id) VALUES ('Wasim Akram', 750000, 3);

INSERT INTO Commissions(employee_id, commission_amount) VALUES (1, 5000);
INSERT INTO Commissions(employee_id, commission_amount) VALUES (2, 3000);
INSERT INTO Commissions(employee_id, commission_amount) VALUES (3, 4000);
INSERT INTO Commissions(employee_id, commission_amount) VALUES (1, 4000);
INSERT INTO Commissions(employee_id, commission_amount) VALUES (2, 3000);
INSERT INTO Commissions(employee_id, commission_amount) VALUES (4, 2000);
INSERT INTO Commissions(employee_id, commission_amount) VALUES (5, 1000);
INSERT INTO Commissions(employee_id, commission_amount) VALUES (6, 5000);

SELECT * FROM Departments;
SELECT * FROM Employees;
SELECT * FROM Commissions;

-- Find the employee who gets the highest total commission.
SELECT name
FROM   Employees
LEFT JOIN Commissions
ON Employees.id = Commissions.employee_id
GROUP  BY Employees.name
ORDER  BY Count(commission_amount) DESC
LIMIT  1;

-- Find employee with 4th Highest salary from employee table.
SELECT GROUP_CONCAT(name), salary
FROM Employees
GROUP BY salary
ORDER BY salary DESC
LIMIT 3,1;

-- Find department that is giving highest commission.
SELECT   Departments.name AS name
FROM     Departments
JOIN     Employees
JOIN     Commissions
where    ((Employees.department_id = Departments.id) AND (Commissions.employee_id = Employees.id))
GROUP BY Departments.name
ORDER BY sum(Commissions.commission_amount) DESC
LIMIT    1;

-- Find employees getting commission more than 3000 Display Output in following pattern: Chris Gayle, Rahul Dravid  4000
SELECT Group_concat(Employees.name),
       Commissions.commission_amount
FROM   Commissions
LEFT JOIN Employees
ON Employees.id = Commissions.employee_id
WHERE Commissions.commission_amount > 3000
GROUP BY Commissions.commission_amount;